const express = require('express')
const mongoose = require('mongoose')

const app = express()
const port = 4000

// Mongoose uses the connect method to connect to the cluster in our mongodb atlas

/*
    It takes 2 arguments:
    1. Connection string from our mongodb atlas
    2. Object that contains the middlewares/standards that MongoDB uses
*/

/*
    newUrlParser - it allows us to avoid any current and/or future error while connecting to mongoDB
    useUnifiedTopology = if 'false' by default. Set to true to opt in to using mongoDB driver's new connection management engine
*/

mongoose.connect(
	`mongodb+srv://ramosmark:admin123@zuitt-batch197.ixuhayw.mongodb.net/S35-Activity?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
)

// Initializes the mongoose connection to the mongodb database by assigning 'mongoose connection' to the db variable
const db = mongoose.connection

// Listens to the events of the connection by using the on method of the monggose connection and logs the details in the console based of the event (error on succesful)
db.on('error', console.error.bind(console, 'Connection'))
db.once('open', () => console.log('Connected to MongoDB'))

// CREATING A SCHEMA
// Define the fields with their corresponding data types
const taskSchema = new mongoose.Schema({
	name: String,
	// Set status to data type string and default value of Pending
	status: {
		type: String,
		default: 'Pending',
	},
})

// MODELS
// The variable/object 'Task' can now be used to run commands for interacting with our DB
// Task is capitalized following the MVC approach for the naming convetions
// Models must be in singular form and capitalized
// The first parameter is used to specify the name of the collection where we will store our data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in our MongoDB collection/s
const Task = mongoose.model('Task', taskSchema)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.post('/tasks', (req, res) => {
	Task.findOne({ name: req.body.name }, (error, result) => {
		if (error) {
			return res.send(error)
		} else if (result !== null && result.name === req.body.name) {
			return res.send('Duplicate task found')
		} else {
			const newTask = new Task({
				name: req.body.name,
			})

			newTask.save((error, savedTask) => {
				if (error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Task Created!')
				}
			})
		}
	})
})

app.get('/tasks', (req, res) => {
	Task.find({}, (error, result) => {
		if (error) {
			return res.send(error)
		} else {
			return res.status(200).json({
				tasks: result,
			})
		}
	})
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
})

const User = mongoose.model('User', userSchema)

app.post('/signup', (req, res) => {
	const requestBody = req.body
	console.log(requestBody)

	User.findOne({ username: requestBody.username }, (error, result) => {
		if (error) {
			return res.send(error)
		} else if (result !== null && result.username === requestBody.username) {
			return res.send('Username already taken')
		} else {
			console.log(requestBody)
			const newUser = new User({
				username: requestBody.username,
				password: requestBody.password,
			})

			console.log(newUser)

			newUser.save(newUser, (error, savedUser) => {
				if (error) {
					return console.error(error)
				} else {
					return res.status(201).send('Registration Complete')
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))
